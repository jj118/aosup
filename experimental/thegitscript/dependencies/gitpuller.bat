@echo off
setlocal ENABLEDELAYEDEXPANSION

set name=%1
set gitbak=%systemdrive%\postinstall\experimental\gitbak\git
set reader=%systemdrive%\postinstall\experimental\thegitscript\handling\reader
set didyoufindone=%systemdrive%\postinstall\experimental\thegitscript\handling\didyoufindone
set moddir=%cd%\%name%

if exist "%moddir%\.git" (
    %gitbak% -C "%moddir%" pull | find /v "Already" > nul
    if errorlevel 9009 (
        echo git executable missing! >>%reader%
    ) else if errorlevel 1 (
        %gitbak% -C "%moddir%" pull
        if !errorlevel! equ 0 (
            echo %name% is newest >>%reader% && echo yeah i found one >%didyoufindone%
            set newest=1
        ) else (
            echo %name% failed >>%reader% && set newest=0
        )
    )  
    if !errorlevel! equ 0 (
        if !newest! neq 1 ( 
            echo updated %name% >>%reader% && echo yeah i found one >%didyoufindone%
        )
    )
)
set newest=0

echo exit called
