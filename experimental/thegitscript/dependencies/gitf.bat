@echo off
setlocal ENABLEDELAYEDEXPANSION

set importantpath=%systemdrive%\postinstall\experimental\thegitscript\dependencies
set reader=%systemdrive%\postinstall\experimental\thegitscript\handling\reader
set didyoufindone=%systemdrive%\postinstall\experimental\thegitscript\handling\didyoufindone
set yorilib=%systemdrive%\postinstall\experimental\thegitscript\dependencies\yorilib\yori.exe

start /b powershell.exe -executionpolicy bypass -windowstyle hidden -noninteractive -nologo -file "C:\postinstall\experimental\thegitscript\dependencies\gitf.ps1"

del %reader%

for /d %%i in (%~dp1*) do (
    start /b %yorilib% /c %importantpath%\gitpuller.bat %%~nxi
)

:: wait (stackoverflow :)
:loopermanliketheflstudio
ping 192.0.2.0 -n 1 -w 150 >nul
tasklist /fi "ImageName eq yori.exe" /fo csv 2>nul | find /I "yori.exe" >nul
if "!errorlevel!"=="0" goto loopermanliketheflstudio
:: if it gets past this, the scripts are done

if exist %didyoufindone% (
    echo. >>%reader%
    echo. done, closing... >> %reader%
    ping 192.0.2.0 -n 1 -w 3000 >nul
    taskkill /im powershell.exe /F
    del %reader%
    del %didyoufindone%
    exit
)

echo closing... nothing found :( >> %reader%
ping 192.0.2.0 -n 1 -w 3000 >nul
taskkill /im powershell.exe /F
del %reader%
del %didyoufindone%
exit
