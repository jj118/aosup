@echo off

timeout 5 /nobreak
cd %SYSTEMDRIVE%\installup
git config --global --add safe.directory %systemdrive%/installup

:looper
if exist %SYSTEMDRIVE%\installup\ACTIVE.ACTIVE (
    git pull
    timeout 10 /nobreak 
    goto looper:
)

if not exist %SYSTEMDRIVE%\installup\ACTIVE.ACTIVE exit